from ROOT import RooFit,RooWorkspace,RooJSONFactoryWSTool,TROOT,Math,RooMsgService, TFile

ws = RooWorkspace("workspace")
tool = RooJSONFactoryWSTool(ws)
tool.importJSON("ratio_likelihood.json")

ws.Print()

model = ws["ModelConfig"]

options = Math.MinimizerOptions
options.SetDefaultMinimizer("Minuit2","Migrad")

result = model.fitTo(ws["obsData"], RooFit.Save(), RooFit.PrintLevel(-1), RooFit.Offset(True), RooFit.Optimize(2), RooFit.Hesse(True))
result.Print()
